FROM golang:1.9.2-stretch

ENV LIBVIPS_VERSION 8.5.9

COPY vips-8.5.9.tar.gz /tmp/

# Installs libvips + required libraries
RUN \
  # Install dependencies
  apt-get update && \
  DEBIAN_FRONTEND=noninteractive apt-get install -y \
  automake build-essential curl \
  gobject-introspection gtk-doc-tools libglib2.0-dev libjpeg62-turbo-dev libpng-dev \
  libwebp-dev libtiff5-dev libgif-dev libexif-dev libxml2-dev libpoppler-glib-dev \
  swig libmagickwand-dev libpango1.0-dev libmatio-dev libopenslide-dev libcfitsio-dev \
  libgsf-1-dev fftw3-dev liborc-0.4-dev librsvg2-dev && \

  # Build libvips
  cd /tmp && \
  tar zvxf vips-8.5.9.tar.gz && \
  cd /tmp/vips-8.5.9 && \
  ./configure --enable-debug=no --without-python $1 && \
  make && \
  make install && \
  ldconfig && \

  # Clean up
  apt-get remove -y curl automake build-essential && \
  apt-get autoremove -y && \
  apt-get autoclean && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN go get -u github.com/go-redis/redis && \
    go get -u github.com/rs/cors && \
    go get -u github.com/tj/go-debug && \
    go get -u gopkg.in/h2non/bimg.v1

# gcc for cgo
RUN apt-get update && apt-get install -y \
    gcc curl git libc6-dev make ca-certificates \
    --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*

COPY . $GOPATH/src/neoderm.com/gary/imageserver
RUN go build -race -o bin/imageserver neoderm.com/gary/imageserver
ENTRYPOINT ["imageserver"]
EXPOSE 8088

#RUN mkdir /image-volume
#RUN cp -a test /image-volume/test
#VOLUME /image-volume
#RUN ls -al /image-volume
