Credit to [`imaginary`](https://github.com/h2non/imaginary)

Custom Image Server with Redis cache.

### Docker Usage
Image
```
hkgarylai/imageserver:1.0.2
```

docker-compose sample
```
version: '3'
services:
  server:
    image: hkgarylai/imageserver:1.0.2
    ports:
      - "8088:8088"
    environment:
      - REDIS_URL=redis:6379
      - REDIS_PASSWORD=YOUR_PASSWORD
      - CACHE_EXPIRE_SECOND=86400
    command: -cors -enable-url-source
    networks:
      - imagenet
  redis:
    image: redis:4.0.2
    ports:
      - "6379:6379"
    deploy:
      placement:
        constraints: [node.role == manager]
    volumes:
      - imagevolume:/data
      - ./redis.conf:/etc/redis.conf
    command: redis-server /etc/redis.conf --requirepass YOUR_PASSWORD
    networks:
      - imagenet
networks:
  imagenet:
volumes:
  imagevolume:

```


redis.conf sample
```
save 60 1000
save 300 10
save 600 1

rdbcompression yes
dbfilename dump.rdb
dir /data

appendonly yes

maxmemory 2G
maxmemory-policy allkeys-lfu
```
